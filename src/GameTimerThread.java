import javax.swing.*;

public class GameTimerThread implements Runnable {

    private final MineSweeperGUI mineSweeperGUI;
    private Thread thread;

    public GameTimerThread(MineSweeperGUI gui)
    {
        this.mineSweeperGUI = gui;
    }

    @Override
    public void run() {
        while (true)
        {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                JOptionPane.showMessageDialog(null, "Thread Interrupted!");
                e.printStackTrace();
                System.exit(-1);
            }
            if (mineSweeperGUI.hasStarted) {
                int currentTime = Integer.parseInt(mineSweeperGUI.timer.getText());
                mineSweeperGUI.timer.setText(String.valueOf(++currentTime));
            }
        }
    }

    // If the thread is not correctly set
    // assign the thread and call the start
    // function
    public void start()
    {
        if (this.thread == null)
        {
            this.thread = new Thread(this);
            thread.start();
        }
    }
}