import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;


public class ButtonHandler implements MouseListener {
    private final int row;
    private final int col;
    private final MineGrid grid;
    private final MineSweeperGUI mineSweeperGUI;

    public ButtonHandler(int x, int y, MineGrid g, MineSweeperGUI mineSweeperGUI)
    {
        row = x;
        col = y;
        grid = g;
        this.mineSweeperGUI = mineSweeperGUI;
    }

    @Override
    public void mouseClicked(MouseEvent event) {

        // if the games has ended in either way do
        // not trigger for anything until a new game
        // starts
        if (!mineSweeperGUI.gameEnd) {

            // allow thread to start counting
            mineSweeperGUI.hasStarted = true;

            // if right-click, either remove the flag that is
            // already places on the cell or put a flag icon if
            // the cell do not have it
            if (SwingUtilities.isRightMouseButton(event))
            {
                if (event.getSource() instanceof JButton)
                {
                    JButton button = (JButton)event.getSource();
                    if ((grid.isOpened(row,col))) {
                        return;//it makes nothing
                    }
                    else if (grid.hasFLAG(row,col))
                    {
                        //remove the flag & set flag info
                        //.
                        grid.setFLAG(row,col);
                        button.setIcon(null);

                        if (grid.isMINE(row,col)) {
                            grid.minesNotFound++;
                        }

                        // increment the number of mines left
                        mineSweeperGUI.minesLeft.setText(
                                String.valueOf(Integer.parseInt(mineSweeperGUI.minesLeft.getText())+1)
                        );
                    }
                    else
                    {
                        // set the flag icon
                        URL imageUrl = ClassLoader.getSystemResource("flagged.png");
                        ImageIcon icon = new ImageIcon(imageUrl);
                        button.setIcon(icon);
                        grid.setFLAG(row,col);

                        // decrement the value on the top left on the frame
                        mineSweeperGUI.minesLeft.setText(
                                String.valueOf(Integer.parseInt(mineSweeperGUI.minesLeft.getText())-1)
                        );

                        // if a mine is flagged, decrement the number of mines
                        // yet to be flagged, this info is not shown to the
                        // user
                        if (grid.isMINE(row,col)) {
                            grid.minesNotFound--;
                        }

                        // if all mines are found and all other cells are revealed
                        // then end the game
                        if (grid.minesNotFound == 0 && grid.revealedCells == grid.numRows * grid.numCols - grid.numMines)
                        {
                            mineSweeperGUI.hasStarted = false;
                            endGameSuccess();
                        }
                    }
                }
            }

            // if left click, reveal the info for that cell
            // if MINE end the game
            else if (SwingUtilities.isLeftMouseButton(event))
            {
                if (grid.isMINE(row,col))
                {
                    // endTheGame function is not called here, because
                    // we have to pass JButton to it.
                    if (event.getSource() instanceof JButton)
                    {
                        mineSweeperGUI.hasStarted = false;
                        mineSweeperGUI.gameEnd = true;

                        JButton button = (JButton)event.getSource();

                        // show other mines
                        mineSweeperGUI.showMines();

                        // set the icon of clicked cell to red mine
                        URL imageUrl = ClassLoader.getSystemResource("bang.png");
                        ImageIcon icon = new ImageIcon(imageUrl);
                        button.setIcon(icon);

                        // change smiley face to sad face
                        imageUrl = ClassLoader.getSystemResource("sad.png");
                        icon = new ImageIcon(imageUrl);
                        Image scaledImage = icon.getImage().getScaledInstance(50, 50, Image.SCALE_FAST);
                        icon = new ImageIcon(scaledImage);
                        mineSweeperGUI.smiley.setIcon(icon);

                        // prompt the message and exit the program
                        JOptionPane.showMessageDialog(null,"OOOPS!!");

                    }
                }
                else
                {
                    if (event.getSource() instanceof JButton) {

                        // if the cell is already opened, do not do anything
                        if (grid.isOpened(row, col)) {
                            return;
                        }

                        // else recursively call updateNeighbors to reveal info
                        else
                        {
                            mineSweeperGUI.updateNeighbors(row,col);
                        }

                        if (grid.minesNotFound == 0 && grid.revealedCells == grid.numRows * grid.numCols - grid.numMines)
                        {
                            mineSweeperGUI.hasStarted = false;
                            endGameSuccess();
                        }
                    }
                }
            }
        }
    }

    // If the game is successfully completed
    // the do all necessary work here
    public void endGameSuccess()
    {
        mineSweeperGUI.gameEnd = true;

        URL imageUrl = ClassLoader.getSystemResource("sunglasses.png");
        ImageIcon icon = new ImageIcon(imageUrl);
        Image scaledImage = icon.getImage().getScaledInstance(50, 50, Image.SCALE_FAST);
        icon = new ImageIcon(scaledImage);
        mineSweeperGUI.smiley.setIcon(icon);

        int timePlayed = Integer.parseInt(mineSweeperGUI.timer.getText());
        if (mineSweeperGUI.updateRecords(timePlayed)) {
            JOptionPane.showMessageDialog(null,"New Record!!! You are genius!!");
        }
        else {
            JOptionPane.showMessageDialog(null,"You are genius!!");
        }
    }


    // other methods I had to overload since MouseListener
    // class is implemented
    public void mousePressed(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
}