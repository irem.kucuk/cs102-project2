import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.*;
import java.net.URL;

public class MineSweeperGUI extends JPanel {
    private MineGrid grid;
    protected JButton[][] buttons;
    protected JButton smiley;
    protected JLabel timer;
    protected JLabel minesLeft;
    protected JPanel mineGridPanel;
    protected boolean hasStarted;
    protected boolean gameEnd;

    int numRows;
    int numCols;
    int numMines;
    int gameType;

    int[] records = new int[4];

    public MineSweeperGUI(int numRows, int numCols, int numMines) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.numMines = numMines;
        this.buttons = new JButton[numRows][numCols];
        this.hasStarted = false;
        this.gameEnd = false;

        // Initialize the grid & set layout
        grid = new MineGrid(numRows, numCols, numMines);
        setLayout(new BorderLayout());

        // Set the menu bar and menu items
        // newGame is a subMenu under the main
        // one
        JMenuBar menuBar = new JMenuBar();
        menuBar.setSize(numCols*25,25);
        JMenu game = new JMenu("Game");
        JMenu help = new JMenu("Help");
        JMenu newGame = new JMenu("New Game");


        // Other features in the menu
        JMenuItem about = new JMenuItem("About");
        JMenuItem records = new JMenuItem("Records");
        JMenuItem exit = new JMenuItem("Exit");

        // Add actionListeners for misc operations
        about.addActionListener(new MiscListener());
        records.addActionListener(new MiscListener());
        exit.addActionListener(new MiscListener());

        // create newGame menu items
        JMenuItem easy = new JMenuItem("Easy");
        JMenuItem medium = new JMenuItem("Medium");
        JMenuItem hard = new JMenuItem("Hard");
        JMenuItem custom = new JMenuItem("Custom");

        // add actionListeners that create new game based on difficulty level
        easy.addActionListener(new NewGameListener(this, 0));
        medium.addActionListener(new NewGameListener(this, 1));
        hard.addActionListener(new NewGameListener(this, 2));
        custom.addActionListener(new NewGameListener(this, 3));

        // add menu items to menubar
        newGame.add(easy);
        newGame.add(medium);
        newGame.add(hard);
        newGame.add(custom);

        game.add(newGame);
        game.add(exit);

        help.add(records);
        help.add(about);

        menuBar.add(game);
        menuBar.add(help);

        // Inner panel hold the mine grid GUI and the timers
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BorderLayout());

        JPanel counterPanel = new JPanel();
        counterPanel.setLayout(new GridLayout(1, 3));

        // this borders are not the same in the HW file but similar
        Border raisedBevelBorder = BorderFactory.createRaisedBevelBorder();
        Border loweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        Border compoundBorder = BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder);
        counterPanel.setBorder(compoundBorder);

        // create the timer, smiley face and the mine count
        timer = new JLabel("0", SwingConstants.CENTER);
        minesLeft = new JLabel(String.valueOf(numMines), SwingConstants.CENTER);
        smiley = new JButton();

        Font font = new Font("Verdana", Font.BOLD, 12);
        timer.setFont(font);
        timer.setForeground(Color.RED);

        minesLeft.setFont(font);
        minesLeft.setForeground(Color.RED);

        // set the smiley face icon
        URL imageUrl = ClassLoader.getSystemResource("smiley.png");
        ImageIcon icon = new ImageIcon(imageUrl);
        Image scaledImage = icon.getImage().getScaledInstance(50, 50, Image.SCALE_FAST);
        icon = new ImageIcon(scaledImage);
        smiley.setIcon(icon);
        smiley.setContentAreaFilled(false);
        smiley.setBorderPainted(false);

        // add them to the panel
        counterPanel.add(minesLeft);
        counterPanel.add(smiley);
        counterPanel.add(timer);

        // initialize the grid GUI
        mineGridPanel = new JPanel();
        mineGridPanel.setLayout(new GridLayout(numRows, numCols));
        mineGridPanel.setBorder(compoundBorder);

        // add the buttons to innerPanel and two dimensional array
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                JButton button = new JButton();
                button.setDoubleBuffered(true);
                buttons[i][j] = button;
                mineGridPanel.add(button);
                button.addMouseListener(new ButtonHandler(i, j, grid, this));
            }
        }

        // add counterPanel and mines to inner panel
        innerPanel.add(counterPanel, BorderLayout.NORTH);
        innerPanel.add(mineGridPanel, BorderLayout.CENTER);

        // add everything to outer panel i.e MineSweeperGUI
        this.add(innerPanel, BorderLayout.CENTER);
        this.add(menuBar, BorderLayout.NORTH);
    }


    // Given a coordinate on the grid, this function
    // recursively reveals the information on the neighboring
    // cells.
    public void updateNeighbors(int x, int y) {

        // If the coordinate is outside the grid or the cell
        // has already revealed the info then there is no need
        // to do work.
        if (grid.isInsideGrid(x, y) && !grid.isOpened(x, y)) {
            int content = grid.getCellContent(x, y);
            URL iconURL = grid.getPngURL(x, y);
            ImageIcon icon = new ImageIcon(iconURL);
            buttons[x][y].setIcon(icon);
            grid.setOpened(x, y);

            // if content is zero all the neighboring cells do
            // not have mine inside. We can safely reveal the
            // information, recursively.
            if (content == 0) {
                updateNeighbors(x + 1, y + 1);
                updateNeighbors(x + 1, y);
                updateNeighbors(x + 1, y - 1);

                updateNeighbors(x, y - 1);
                updateNeighbors(x, y + 1);

                updateNeighbors(x - 1, y - 1);
                updateNeighbors(x - 1, y);
                updateNeighbors(x - 1, y + 1);
                //buttons[x][y].setBorderPainted(false);
                buttons[x][y].setContentAreaFilled(false);
                buttons[x][y].setFocusable(false);
            }

            // These are unnecessary yet ...
            mineGridPanel.revalidate();
            mineGridPanel.repaint();

        }
    }

    // If the game ends with a fail, this function reveals the positions
    // of all the mines on the grid. Their icons are turned to a bomb icon.
    public void showMines() {
        URL iconURL = ClassLoader.getSystemResource("bomb.png");
        ImageIcon icon = new ImageIcon(iconURL);
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                if (grid.isMINE(i, j)) {
                    buttons[i][j].setIcon(icon);
                }
            }
        }
    }

    // This function reads the records from the txt file
    // and updates the information in the records array
    public boolean updateRecords(int time)
    {
        boolean isNewRecord = false;
        setRecords();
        if ( time < records[gameType] ) {
            records[gameType] = time;
            isNewRecord = true;
        }
        try {
            BufferedWriter writer = new BufferedWriter( new FileWriter("records.txt"));
            writer.write( records[0] + "\n" +
                    records[1] + "\n" +
                    records[2] + "\n");

            writer.flush();
        } catch (IOException ignored) {}
        return isNewRecord;
    }

    // If the game successfully ends, then this function checks if the
    // user has broken a record. If yes, then records.txt is updated,
    // records are overwritten
    private void setRecords()
    {
        try {
            FileReader reader = new FileReader("records.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line;
            int i = 0;
            while ((line = bufferedReader.readLine()) != null) {
                records[i++] = Integer.parseInt(line);
            }
            reader.close();
        } catch (IOException ignored) {}
    }


    // This listener class is to change the game difficulty level
    // when needed. It rearranges the grid, GUI and resets anything
    // previously used. If the requested game is custom, then user
    // has to input how many columns, rows and mines are wanted
    public class NewGameListener implements ActionListener {
        int gameType;
        MineSweeperGUI gui;

        public NewGameListener(MineSweeperGUI mineSweeperGUI, int gameType) {
            this.gameType = gameType;
            this.gui = mineSweeperGUI;
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            mineGridPanel.setVisible(false);
            Component[] componentList = mineGridPanel.getComponents();

            for (Component c : componentList)
            {
                if (c instanceof JButton)
                {
                    mineGridPanel.remove(c);
                }
            }

            switch (gameType) {
                // easy
                case 0:
                    gui.gameType = 0;
                    changeGrid(9,9,10);
                    break;

                // hard
                case 2:
                    gui.gameType = 2;
                    changeGrid(16,30,99);
                    break;

                //custom
                case 3:
                    int rows = 16;
                    int cols = 16;
                    int mines = 40;
                    try {
                        rows = Integer.parseInt(JOptionPane.showInputDialog(null, "Number of Rows :", 16));
                        cols = Integer.parseInt(JOptionPane.showInputDialog(null, "Number of Columns :", 16));
                        mines = Integer.parseInt(JOptionPane.showInputDialog(null, "Number of Mines :", 40));
                    } catch (Exception ignored) {
                        // If an error occurs reading input from the user, then use medium difficulty
                        JOptionPane.showMessageDialog(null, "Could not create custom game. Give integer inputs");
                        gui.gameType = 1;
                        changeGrid(rows,cols,mines);
                        break;
                    }
                    JOptionPane.showMessageDialog(null, "Custom games are not recorded!");
                    gui.gameType = 3;
                    changeGrid(rows,cols,mines);
                    break;

                // medium
                default:
                    gui.gameType = 1;
                    changeGrid(16,16,40);
                    break;
            }

            URL imageUrl = ClassLoader.getSystemResource("smiley.png");
            ImageIcon icon = new ImageIcon(imageUrl);
            Image scaledImage = icon.getImage().getScaledInstance(50, 50, Image.SCALE_FAST);
            icon = new ImageIcon(scaledImage);
            smiley.setIcon(icon);
            smiley.setContentAreaFilled(false);
            smiley.setBorderPainted(false);

            // some of the functions here are unnecessary but
            // I am a bit lazy to find out which ones
            mineGridPanel.revalidate();
            mineGridPanel.repaint();
            mineGridPanel.setVisible(true);
            JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(gui);
            topFrame.setSize(gui.numCols * 25 , gui.numRows*25 + 25 + 100);
            topFrame.repaint();
            topFrame.revalidate();
            gui.repaint();
            gui.revalidate();
        }

        // This is the main part of the transmission. All the attributes
        // of GUI are reassigned to desired values based on the difficulty
        // level of the new game
        private void changeGrid(int rows, int cols, int mines)
        {
            gui.numMines = mines;
            gui.numRows = rows;
            gui.numCols = cols;

            gui.hasStarted = false;
            gui.gameEnd = false;

            minesLeft.setText(String.valueOf(mines));
            timer.setText("0");

            mineGridPanel.setLayout(new GridLayout(numRows, numCols));
            gui.grid = new MineGrid(rows,cols,mines);

            gui.buttons = new JButton[rows][cols];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    JButton button = new JButton();
                    buttons[i][j] = button;
                    mineGridPanel.add(button);
                    button.addMouseListener(new ButtonHandler(i, j, grid, gui));
                }
            }
        }

    }


    // This class is miscellaneous listener class. It takes care of
    // 3 different types of buttons namely exit, about and records.
    public class MiscListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String itemText = ((JMenuItem)e.getSource()).getText();
            switch (itemText) {
                case "About":
                    JOptionPane.showMessageDialog(null, "          MineSweeper\n" +
                            "Ozyegin University - CS102\n" +
                            "           İrem Küçük  ");

                    break;
                case "Exit":
                    System.exit(0);
                case "Records":
                    setRecords();

                    JOptionPane.showMessageDialog(null, "Easy: " + records[0] + "\n" +
                            "Medium: " + records[1] + "\n" +
                            "Hard: " + records[2]);
                    break;
            }
        }
    }
}