import javax.swing.*;
import java.net.URL;

public class MineSweeper {

    private static final int NUM_MINES = 10;
    private static final int SIZE = 20;

    public static void main(String[] args)
    {
        // Create a frame and call GUI class which starts controller & model
        JFrame frame = new JFrame("Mine Sweeper");
        MineSweeperGUI mineSweeperGUI = new MineSweeperGUI(SIZE,SIZE,NUM_MINES);
        mineSweeperGUI.gameType = 3;
        frame.add(mineSweeperGUI);

        // set the frame icon
        URL iconURL = ClassLoader.getSystemResource("bomb.png");
        ImageIcon icon = new ImageIcon(iconURL);
        frame.setIconImage(icon.getImage());

        // Separate thread to handle time in the game
        GameTimerThread thread = new GameTimerThread(mineSweeperGUI);
        thread.start();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(SIZE*25,600);
        frame.setVisible(true);

    }
}