import java.net.URL;
import java.util.Random;

public class MineGrid  {

    private static final int MINE = -1;
    private final int[][] mineInformation;
    private final boolean[][] flagInformation;
    private final boolean[][] isOpenedInformation;

    int numRows;
    int numCols;
    int numMines;
    int minesNotFound; // how many mines are yet to be found, necessary to end the game
    int revealedCells;

    public MineGrid(int numRows, int numCols, int numMines)
    {
        this.numRows = numRows;
        this.numCols = numCols;
        this.numMines = numMines;
        this.minesNotFound = numMines;
        this.revealedCells = 0;

        mineInformation = new int[numRows][numCols];
        flagInformation = new boolean[numRows][numCols];
        isOpenedInformation = new boolean[numRows][numCols];

        initializeCells();
        placeMines(numMines);
        setMineInformation();
    }


    // taken from slides
    private void initializeCells()
    {
        for (int i = 0; i < mineInformation.length; i++)
        {
            for (int j = 0; j < mineInformation[0].length; j++)
            {
                mineInformation[i][j] = 0;
                flagInformation[i][j] = false;
                isOpenedInformation[i][j] = false;
            }
        }
    }

    // place mines randomly, taken from slides
    private void placeMines(int numMines)
    {
        Random random = new Random();

        for (int i = 0; i < numMines; i++)
        {
            int r = random.nextInt(mineInformation.length);
            int c = random.nextInt(mineInformation[0].length);

            if (mineInformation[r][c] != MINE) {
                mineInformation[r][c] = MINE;
            }
            else
                i--;
        }
    }

    // bunch of helper functions
    public boolean isMINE(int i, int j)    { return mineInformation[i][j] == MINE;          }
    public boolean hasFLAG(int i, int j)   { return flagInformation[i][j];                  }
    public void setFLAG(int i, int j)      { flagInformation[i][j] = !flagInformation[i][j];}
    public boolean isOpened(int i, int j)  { return isOpenedInformation[i][j];              }
    public void setOpened(int i, int j)    {
        isOpenedInformation[i][j] = true;
        revealedCells++;
    }

    // THIS FUNCTION IS NOT WRONG and is taken from the lecture slides
    private void setMineInformation()
    {
        for (int i = 0; i < mineInformation.length; i++)
        {
            for (int j = 0; j < mineInformation[0].length; j++)
            {
                if (mineInformation[i][j] == MINE)
                {
                    // previous row
                    incrementMineCountAt(i-1,j-1);
                    incrementMineCountAt(i-1,j);
                    incrementMineCountAt(i-1,j+1);

                    // left & right cells
                    incrementMineCountAt(i,j-1);
                    incrementMineCountAt(i,j+1);

                    // next row
                    incrementMineCountAt(i+1,j-1);
                    incrementMineCountAt(i+1,j);
                    incrementMineCountAt(i+1,j+1);
                }
            }
        }
    }

    // Helper function to adjust information on the grid
    private void incrementMineCountAt(int i, int j)
    {
        if (isInsideGrid(i,j) && !isMINE(i,j))
        {
            mineInformation[i][j]++;
        }
    }

    // Check if the given coordinate is valid
    public boolean isInsideGrid(int i, int j)
    {
        return (i >= 0 && i < mineInformation.length && j>= 0 && j < mineInformation[0].length);
    }

    // get the information from the given coordinate
    public int getCellContent(int i, int j)
    {
        return mineInformation[i][j];
    }


    // This function looks up to content of the given coordinate
    // and returns the URL to the icon inside Icons directory
    public URL getPngURL(int i, int j)
    {
        int content = getCellContent(i,j);
        URL imageUrl;
        switch (content)
        {
            case 1:
                imageUrl = ClassLoader.getSystemResource("1.png");
                break;
            case 2:
                imageUrl = ClassLoader.getSystemResource("2.png");
                break;
            case 3:
                imageUrl = ClassLoader.getSystemResource("3.png");
                break;
            case 4:
                imageUrl = ClassLoader.getSystemResource("4.png");
                break;
            case 5:
                imageUrl = ClassLoader.getSystemResource("5.png");
                break;
            case 6:
                imageUrl = ClassLoader.getSystemResource("6.png");
                break;
            case 7:
                imageUrl = ClassLoader.getSystemResource("7.png");
                break;
            case 8:
                imageUrl = ClassLoader.getSystemResource("8.png");
                break;
            default:
                imageUrl = ClassLoader.getSystemResource("0.png");
                break;
        }
        return imageUrl;
    }
}